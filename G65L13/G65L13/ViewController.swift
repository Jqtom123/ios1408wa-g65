//
//  ViewController.swift
//  G65L13
//
//  Created by Ivan Vasilevich on 9/25/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit
import Parse
import ParseUI


class ViewController: UIViewController {

	@IBOutlet weak var embedMapVC: UIView!
	@IBOutlet weak var embedTableVC: UIView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		
	}
	
	func fetchCars() {
		let query = PFQuery(className: "Car")
		query.whereKey("owner", equalTo: PFUser.current())
		query.findObjectsInBackground { (objects, error) in
			guard let objects = objects else {
				return
			}
			let model = objects.first!["model"] as! String
			print("firs model we get from DB", model)
		}
	}
	
	func addCar() {
		guard let currentUser = PFUser.current() else {
			return
		}
		let car = PFObject(className: "Car")
		car["model"] = "mark II"
		car["mark"] = "Toyota"
		car["owner"] = currentUser
		car.saveEventually()
	}
	
	@IBAction func viewsSwitchChanged(_ sender: UISegmentedControl) {
		
		switch sender.selectedSegmentIndex {
		case 0:
			embedMapVC.isHidden = false
			embedTableVC.isHidden = true
		case 1:
			embedMapVC.isHidden = true
			embedTableVC.isHidden = false
		default:
			print("unknown case")
			assert(false, "hi i'm seek")
		}
		
		fetchCars()
		checkLogin()
		addCar()
	
	}


}

extension ViewController: PFLogInViewControllerDelegate {
	
	func log(_ logInController: PFLogInViewController, didLogIn user: PFUser) {
		print("welcome", user.username!)
		navigationItem.title = user.username!
		dismiss(animated: true, completion: nil)
	}
	
	func checkLogin() {
		
		if let currentUser = PFUser.current() {
//			log(PFLogInViewController(), didLogIn: currentUser)
		}
		else {
			// Show the signup or login screen
			let loginVC = PFLogInViewController()
//			let facebookLoginButton = FBSDKLoginButton()
//			facebookLoginButton.frame.origin = CGPoint(x: 20, y: 20)
			//			facebookLoginButton.setTitle("Login with facebook", for: .normal)
			//			facebookLoginButton.setTitle("", for: .highlighted)
			//
			//			facebookLoginButton.addTarget(self, action: #selector(showFacebookLogin(from:)), for: .touchUpInside)
			//			facebookLoginButton.backgroundColor = .red
			//			let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
			//			fbLoginManager.loginBehavior = FBSDKLoginBehavior.Web
			//			fbLoginManager.logInWithReadPermissions(["public_profile","email"], fromViewController: self) { (result, error) -> Void in
			//				if error != nil {
			//					print(error.localizedDescription)
			//					self.dismissViewControllerAnimated(true, completion: nil)
			//				} else if result.isCancelled {
			//					print("Cancelled")
			//					self.dismissViewControllerAnimated(true, completion: nil)
			//				} else {
			//
			//				}
			//			}
			
			
//			facebookLoginButton.delegate = self
//			loginVC.view.addSubview(facebookLoginButton)
			loginVC.delegate = self
			loginVC.signUpController?.delegate = self
			//			loginVC.de
			present(loginVC, animated: true, completion: nil)
		}
	}
	
	@objc func showFacebookLogin(from viewController: UIViewController) {
		print("fb button pressed")
		
	}
	
}


extension ViewController: PFSignUpViewControllerDelegate {
	
	func signUpViewController(_ signUpController: PFSignUpViewController, didSignUp user: PFUser) {
		log(PFLogInViewController(), didLogIn: user)
	}
}

