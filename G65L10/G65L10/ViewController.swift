//
//  ViewController.swift
//  G65L10
//
//  Created by Ivan Vasilevich on 9/13/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	
	@IBOutlet weak var imageView: UIImageView!
	

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		var array = [31, 33, 11, 1, 9]
		array.sort { (a, b) -> Bool in
			return a > b
		}
		print(array)
		
	}

	@IBAction func imagePressed(_ sender: UITapGestureRecognizer) {
		presentPicker()
	}
	
	@IBAction func pinchRecognized(_ sender: UIPinchGestureRecognizer) {
		guard let view = sender.view else {
			return
		}
		view.tag = 3
		view.frame.size.width *= sender.scale
		view.frame.size.height *= sender.scale
		sender.scale = 1
	}

	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		guard let view = view.viewWithTag(3), let firstTouch = touches.first else {
			return
		}
		let position = firstTouch.location(in: self.view)
		UIView.animate(withDuration: 0.3) {
			view.center = position
			self.view.backgroundColor = UIColor(red: CGFloat(arc4random_uniform(256))/255,
												green: CGFloat(arc4random_uniform(256))/255,
												blue: CGFloat(arc4random_uniform(256))/255,
												alpha: 1)
			
		}
		
//		UIView.an
		
		
	}
}


extension ViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
	
	func presentPicker() {
		let picker = UIImagePickerController()
		picker.delegate = self
//		picker.sourceType = .camera
		if UIDevice.current.userInterfaceIdiom == .phone {
			present(picker, animated: true, completion: nil)
		}
		else {
			// present picker popover
		}
	}
	
	func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
		log()
		let image = info[UIImagePickerControllerOriginalImage] as! UIImage
		log()
		imageView.image = image
		log()
		picker.dismiss(animated: true) {
			log()
			self.view.backgroundColor = UIColor(red: CGFloat(arc4random_uniform(256))/255,
												green: CGFloat(arc4random_uniform(256))/255,
												blue: CGFloat(arc4random_uniform(256))/255,
												alpha: 1)
		}
		log()
	}
	
}

