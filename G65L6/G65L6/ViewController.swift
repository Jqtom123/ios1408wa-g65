//
//  ViewController.swift
//  G65L6
//
//  Created by Ivan Vasilevich on 8/30/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	
	@IBOutlet weak var imageView: UIImageView!
	@IBOutlet weak var centerLabel: UILabel!
	var someValue: Bool = false
	var clickCounter = 0
	let clickCount = "clickCount"

	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		clickCounter = UserDefaults.standard.integer(forKey: clickCount)
//		bar(isOld: true)
//		bar(isOld: false)
		someValue = true
		bar2()
		someValue = false
		bar(isOld: false)
		bar2()
		buttonPressed()
	}
	
	
	func bar(isOld: Bool) {
		if isOld {
			print("bar is old")
		}
		else {
			print("new bar")
		}
		someValue = true
	}
	
	func bar2() {
		if someValue {
			print("bar2 is old")
		}
		else {
			print("new bar2")
		}
	}

	func changeCounterWithColor(_ color: UIColor) {
		clickCounter += 1
		UserDefaults.standard.set(clickCounter, forKey: clickCount)
		let text = "clickCounter = \(clickCounter)"
		centerLabel.text = text
		view.backgroundColor = color
	}
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		changeCounterWithColor(.yellow)
	}
	
	@IBAction func buttonPressed() {
//		let randomColor = UIColor(red: CGFloat(arc4random_uniform(256))/255,
//								  green: CGFloat(arc4random_uniform(256))/255,
//								  blue: CGFloat(arc4random_uniform(256))/255,
//								  alpha: 1)
		//35 34 43
		let randomColor = UIColor.init(displayP3Red: 0.35, green: 0.34, blue: 0.43, alpha: 1)
		changeCounterWithColor(randomColor)
	}
	
	@IBAction func funnyButtonPressed(_ sender: UIButton) {
		print(sender.tag)
		let image = UIImage.init(named: "fatBike")
		imageView.image = image
	}
	
}

