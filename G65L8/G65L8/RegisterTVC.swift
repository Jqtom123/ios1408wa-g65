//
//  RegisterTVC.swift
//  G65L8
//
//  Created by Ivan Vasilevich on 9/6/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class RegisterTVC: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		var count = super.tableView(tableView, numberOfRowsInSection: section)
		if section == 0 {
			count -= 2
		}
		return count
	}
	
	override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		var height = super.tableView(tableView, heightForRowAt: indexPath)
		if indexPath.section == 0 && indexPath.row == 1 {
			height = 0
		}
		return height
	}
	

}
